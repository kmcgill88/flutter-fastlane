# flutter-fastlane

A Flutter image that includes the following clis:
1. `flutter`
1. `fastlane`
1. `az` (Azure)

# Versions
1. Flutter 3.10.6: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.10.6`
1. Flutter 3.13.0: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.13.0`
1. Flutter 3.13.1: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.13.1`
1. Flutter 3.16.5: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.16.5`
1. Flutter 3.19.5: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.19.5`
1. Flutter 3.19.6: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.19.6`
1. Flutter 3.22.1: `registry.gitlab.com/kmcgill88/flutter-fastlane:3.22.1`