#!/usr/bin/env sh
set -e

FLUTTER_VERSION=3.22.1

echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$CI_REGISTRY" --password-stdin
docker build --build-arg FLUTTER_VERSION=${FLUTTER_VERSION} --tag "$CI_REGISTRY_IMAGE:${FLUTTER_VERSION}" --tag "$CI_REGISTRY_IMAGE:latest" .
docker push "$CI_REGISTRY_IMAGE:${FLUTTER_VERSION}"
docker push "$CI_REGISTRY_IMAGE:latest"
